
import pefile
import sys
import os
import optparse
from ctypes import wintypes

#pe.DIRECTORY_ENTRY_IMPORT[0].imports[1].name
# funtion calls for each dll imported
dlltree=[]
processed = []
header = ""
labels = ""
paths = []
knowndlls=[]
working_appdir = ""
sysdir = ""

def process_bin(binary):
  global tabs
  global dlltree
  global labels
  global paths
  parent,fname = filepath(binary)
  if parent != '':
    # create new paths containing parent, and append global paths to it
    new_path = paths
    new_path.insert(0,parent)
    pass
  else:
    new_path=paths

  if fname not in processed:
    exists = set_colors(new_path,fname,binary)
    if exists == False:
      labels += '"{0}" [color="green"];\n'.format(fname)
    else:
      processed.append(fname)
      imports = get_imports(fname)
      try:
        for subdll in imports:
          dlltree +=  ('"{0}" -> "{1}";\n'.format(fname,subdll),)
          process_bin(subdll)
      except Exception,e:
        pass

def init_paths():
  """ Windows searches 1)Appdir, 2)System dir, 3) 16bit System dir,
  4) Windows dir, 5) Current dir, 6) syspath """

  global knowndlls
  global paths
  knowndlls = set_knowndlls()
  paths = get_syspaths()

def get_syspaths():
  import win32api
  global sysdir
  path = win32api.GetEnvironmentVariable('PATH')
  sysdir = win32api.GetSystemDirectory()
  paths = [win32api.GetSystemDirectory(), win32api.GetWindowsDirectory()] + path.split(';')
  return paths

def set_knowndlls():
    ''' This should be called once at or near startup to set the knowndlls'''
    ### fixme need to also filter and return directorys
    import _winreg
    knowndlls = []
    hklm = _winreg.ConnectRegistry(None,_winreg.HKEY_LOCAL_MACHINE)
    knowndll_key = _winreg.OpenKey(hklm, r"SYSTEM\CurrentControlSet\Control\Session Manager\KnownDLLs")
    for i in xrange(0,_winreg.QueryInfoKey(knowndll_key)[1]-1):
        knowndlls.append(_winreg.EnumValue(knowndll_key, i)[1].lower())
    return knowndlls

def set_colors(new_path, fname, binary):
  global labels
  global knowndlls
  if fname.lower() in knowndlls:
    labels += '"{0}" [color="red"];\n'.format(fname)
    return True
  if in_memory(binary):
    labels += '"{0}" [color="orange"];\n'.format(fname)
    return True
#   if file_locked(os.path.join(new_path,fname)):
#     labels += '"{0}" [color="grey"];\n'.format(fname)
  exists,hijackable = file_exists(new_path,fname)
  if hijackable:
    labels += '"{0}" [color="yellow"];\n'.format(fname)
    return exists
  return True
 
def filepath(binary):
  """ Returns a tuple containing (path,file). If only a filename was given: (,file)"""
  return ("\\".join(binary.split('\\')[:-1]),binary.split('\\')[-1])

def get_imports(filename):
  global paths
  imports = []
  for path in paths:
    try:
      fname = os.path.join(path,filename)
      if os.path.exists(fname):
        try:
          pe = pefile.PE(fname)
          for entry in pe.DIRECTORY_ENTRY_IMPORT:
            imports.append(entry.dll)
        except Exception,e:
          pass
        return imports
    except Exception,e:
      #print str(e)
      pass


def file_exists(paths,filename):
  global working_appdir
  global sysdir
  exists,hijackable=False,False
  for path in paths:
    if os.path.isfile(os.path.join(path,filename)):
        exists = True
        if os.path.isfile(os.path.join(sysdir,filename)):
          if filename not in knowndlls:
            if not os.path.isfile(os.path.join(working_appdir,filename)):
              hijackable = True
  return(exists,hijackable)

def build_digraph():
  digraph = 'digraph G_component_0 {\n'
  digraph += 'graph [ranksep=5, root="test"];\n'
  digraph += 'ration=fill;'
  digraph += 'node [style=filled];'
  digraph += 'sep="10";smoothing="1";'

  
  global dlltree
  global labels
  global header
  digraph += header
  for set in dlltree:
    digraph += set
  digraph += labels
  digraph +="}"
  return digraph

def in_memory(binary):
  '''Check if module is already loaded in memory'''
  # This should append to a list so future checks are faster  wintypes.windll.kernel32.GetModuleHandleW.restype = wintypes.HMODULE
  wintypes.windll.kernel32.GetModuleHandleW.argtypes = [wintypes.LPCWSTR]
  hMod = wintypes.windll.kernel32.GetModuleHandleW(binary)
  if hMod:return True
  else:return False

def file_locked(binary):
  try:
    f = open(binary,'r')
    f.close()
    return False
  except IOError:
    return True

def get_processes():
  import psutil
  global header
  global working_appdir
  processes = []
  procs = psutil.get_process_list()

  for proc in procs:
    try:
      processes.append(proc.exe())
      #print ('\\').join(proc.exe.split('\\')[:-1])
      #print proc.exe.split('\\')[-1]
    except:pass
  
  for process in processes:
    header += '"test" -> "{0}";\n'.format(process.split('\\')[-1])
    working_appdir = '\\'.join(process.split('\\')[:-1])
    process_bin(process)

if __name__ == "__main__":
  
  parser = optparse.OptionParser("Usage {0} -b <binary> -w <output to file>\nOR {0} -d To use running processes".format(sys.argv[0]))
  parser.add_option("-b", dest="binary", type="string", help="The binary to process. Do not enter full path as this will search the default path")
  parser.add_option("-w", dest="outfile", type="string", help="If you want to send to a file instead of stdout")
  parser.add_option("-d", "--dump", action="store_true", help="If you want to send to a file instead of stdout")

  (options,args) = parser.parse_args()
  init_paths()
  if len(sys.argv) <= 1:
    get_processes()
    digraph = build_digraph()
    with open('output.dot', 'w') as f:
      f.write(digraph)
   
  if options.dump:
    get_processes()
    digraph = build_digraph()
    
  elif options.binary:
    binary = options.binary
    process_bin(binary)
    digraph = build_digraph()
#  else:
#    sys.exit(parser.usage)
  if options.outfile:
    outfile = options.outfile
    with open(outfile,'w+') as outfile:
      outfile.write(digraph)
  else:
    print digraph


